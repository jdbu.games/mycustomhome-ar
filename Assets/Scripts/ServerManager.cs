using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class ServerManager : MonoBehaviour
{
    [SerializeField] private string jsonURL;
    [SerializeField] private ItemButton itemButtonManager;
    [SerializeField] private GameObject buttonsContainer;

    private PopUpConfigurations popUpConfigurations;
    private PopUpFactory popUpFactory;
    private LoadingView currentLoading;
    private AlertView currentAlert;

    [Serializable]
    public struct Items
    {
        [Serializable]
        public struct Item
        {
            public string name;
            public string description;
            public string uRLBundleModel;
            public string uRLImageModel;
            public string detectionMode;
        }

        public Item[] items;
    }

    public Items newItemsCollection = new Items();

    private void Start()
    {
        GameManager.instance.OnItemsMenu += CreateButtons;

        popUpConfigurations = Resources.Load<PopUpConfigurations>("PopUps/PopUpConfigurations");
        popUpFactory = new PopUpFactory(Instantiate(popUpConfigurations));

        StartCoroutine(GetJsonData());
    }

    private void CreateButtons()
    {
        Debug.Log("CREATE BUTTONS");
        foreach (var item in newItemsCollection.items)
        {
            ItemButton itemButton = Instantiate(itemButtonManager, buttonsContainer.transform);
            itemButton.name = item.name;
            itemButton.ItemName = item.name;
            itemButton.ItemDescription = item.description;
            itemButton.URLBundleModel = item.uRLBundleModel;
            StartCoroutine(GetBundleImage(item.uRLImageModel, itemButton));
            itemButton.DetectionMode = item.detectionMode;
        }

        GameManager.instance.OnItemsMenu -= CreateButtons;
    }

    private IEnumerator GetJsonData()
    {
        currentLoading = popUpFactory.CreateLoading();

        UnityWebRequest serverRequest = UnityWebRequest.Get(jsonURL);

        yield return serverRequest.SendWebRequest();

        Destroy(currentLoading.gameObject);

        if (serverRequest.result == UnityWebRequest.Result.Success)
        {
            Debug.Log(serverRequest.downloadHandler.text);
            newItemsCollection = JsonUtility.FromJson<Items>(serverRequest.downloadHandler.text);
        }
        else
        {
            Debug.Log("ERROR: " + serverRequest.error);
            StartCoroutine(GetJsonData());
        }
    }

    private IEnumerator GetBundleImage(string uRLImage, ItemButton button)
    {
        UnityWebRequest serverRequest = UnityWebRequestTexture.GetTexture(uRLImage);

        yield return serverRequest.SendWebRequest();

        if (serverRequest.result == UnityWebRequest.Result.Success)
        {
            button.ImageBundle.texture = ((DownloadHandlerTexture)serverRequest.downloadHandler).texture;
        }
        else
        {
            Debug.LogError("ERROR: " + serverRequest.error);
        }
    }
}

