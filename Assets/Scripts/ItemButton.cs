using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ItemButton : MonoBehaviour
{
    public Action<ItemButton> OnButtonPressed;
    public Action<ItemButton> On3DModelCreated;

    private string itemName;
    private string itemDescription;
    private string uRLBundleModel;
    private RawImage imageBundle;
    private string detectionMode;

    private ARInteractionsManager aRInteractionManager;
    private ARPlaneManager aRPlaneManager;

    public string ItemName { get => itemName; set => itemName = value; }
    public string ItemDescription { set => itemDescription = value; }
    public string URLBundleModel { set => uRLBundleModel = value; }
    public RawImage ImageBundle { get => imageBundle; set => imageBundle = value; }
    public string DetectionMode { get => detectionMode; set => detectionMode = value; }

    private PopUpConfigurations popUpConfigurations;
    private PopUpFactory popUpFactory;
    private LoadingView currentLoading;
    private AlertView currentAlert;


    // Start is called before the first frame update
    void Start()
    {
        transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = itemName;
        imageBundle = transform.GetChild(1).GetComponent<RawImage>();
        transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = itemDescription;

        var button = GetComponent<Button>();
        button.onClick.AddListener(Create3DModel);
        button.onClick.AddListener(GameManager.instance.ARPosition);

        aRInteractionManager = FindObjectOfType<ARInteractionsManager>();
        aRPlaneManager = FindObjectOfType<ARPlaneManager>();

        popUpConfigurations = Resources.Load<PopUpConfigurations>("PopUps/PopUpConfigurations");
        popUpFactory = new PopUpFactory(Instantiate(popUpConfigurations));
    }

    private void Create3DModel()
    {
        SetDetectionMode();
        GameObject modelLoaded = aRInteractionManager.GetModelLoaded(ItemName);
        if(modelLoaded == null)
        {
            print("Null");
            StartCoroutine(DownLoadAssetBundle(uRLBundleModel));
        }
        else if(!modelLoaded.activeInHierarchy)
        {
            print("Loaded");
            aRInteractionManager.Item3DModel = modelLoaded;
        }
        else
        {
            print("Duplicate");
            GameObject newModel = Instantiate(modelLoaded);
            newModel.name = itemName;
            aRInteractionManager.Item3DModel = newModel;
        }
    }

    private void SetDetectionMode()
    {
        print("SetDetectionMode: " + DetectionMode);
        switch (DetectionMode)
        {
            case "Vertical":
                aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.Vertical;
                break;

            case "Horizontal":
                aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.Horizontal;
                break;
        }

        aRInteractionManager.SetARPointer(aRPlaneManager.requestedDetectionMode);
    }

    IEnumerator DownLoadAssetBundle(string uRLAssetBundle)
    {
        currentLoading = popUpFactory.CreateLoading();

        UnityWebRequest serverRequest = UnityWebRequestAssetBundle.GetAssetBundle(uRLAssetBundle);
        
        yield return serverRequest.SendWebRequest();

        Destroy(currentLoading.gameObject);

        if (serverRequest.result == UnityWebRequest.Result.Success)
        {
            AssetBundle model3D = DownloadHandlerAssetBundle.GetContent(serverRequest);
            print(model3D);
            if(model3D != null)
            {
                aRInteractionManager.Item3DModel = Instantiate(model3D.LoadAsset(model3D.GetAllAssetNames()[0]) as GameObject);
                aRInteractionManager.Item3DModel.name = itemName;
            }
            else
            {
                Debug.Log("Not a valid Assets Bundle");
            }
        }
        else
        {
            Debug.Log("Error x`C");
            StartCoroutine(DownLoadAssetBundle(uRLBundleModel));
        }
    }
}
