using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARInteractionsManager : MonoBehaviour
{
    [SerializeField] private Camera aRCamera;
    private ARRaycastManager aRRaycastManager;
    private readonly List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private GameObject aRPointer;
    private GameObject item3DModel;
    private GameObject itemSelected;

    [SerializeField] private List<GameObject> modelsLoaded;

    private bool isInitialPosition;
    private bool isOverUI;
    private bool isOver3DModel;

    private Vector2 initialTouchPos;

    private PlaneDetectionMode detectionMode;

    public GameObject Item3DModel
    {
        get
        {
            return item3DModel;
        }

        set
        {
            item3DModel = value;
            item3DModel.SetActive(true);
            item3DModel.transform.position = aRPointer.transform.position;
            item3DModel.transform.parent = aRPointer.transform;
            isInitialPosition = true;

            if (!CheckModelsLoaded(item3DModel))
                modelsLoaded.Add(item3DModel);

            print("OK");
        }
    }

    private bool CheckModelsLoaded(GameObject newModel)
    {
        foreach (GameObject model in modelsLoaded)
        {
            if (model.name == newModel.name)
            {
                return true;
            }
        }

        return false;
    }

    public GameObject GetModelLoaded(string modelName)
    {
        foreach (GameObject model in modelsLoaded)
        {
            if(model.name == modelName)
            {
                return model;
            }
        }

        return null;
    }

    public void SetARPointer(PlaneDetectionMode planeDetectionMode)
    {
        detectionMode = planeDetectionMode;
        switch (planeDetectionMode)
        {
            case PlaneDetectionMode.None:
                break;
            case PlaneDetectionMode.Horizontal:
                //LeanTween.rotateLocal(aRPointer, new Vector3(90f, 0f, 0f), 0f);
                break;
            case PlaneDetectionMode.Vertical:
                //LeanTween.rotateLocal(aRPointer, Vector3.zero, 0f);
                break;
            default:
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        aRPointer = transform.GetChild(0).gameObject;
        aRRaycastManager = FindObjectOfType<ARRaycastManager>();
        GameManager.instance.OnMainMenu += SetItemPosition;
    }

    // Update is called once per frame
    void Update()
    {
        print("Rotation: " + transform.rotation);
        if(isInitialPosition)
        {
            Vector2 middlePointScreen = new Vector2(Screen.width / 2, Screen.height / 2);
            aRRaycastManager.Raycast(middlePointScreen, hits, TrackableType.Planes);
            if(hits.Count > 0)
            {
                transform.SetPositionAndRotation(hits[0].pose.position, hits[0].pose.rotation);
                //transform.position = hits[0].pose.position;
                aRPointer.SetActive(true);
                isInitialPosition = false;
            }
        }

        if(Input.touchCount > 0)
        {
            Touch touchOne = Input.GetTouch(0);
            if(touchOne.phase == TouchPhase.Began)
            {
                var touchPosition = touchOne.position;
                isOverUI = IsTapOverUI(touchPosition);
                isOver3DModel = IsTapOver3DModel(touchPosition);
            }

            if(touchOne.phase == TouchPhase.Moved)
            {
                if(aRRaycastManager.Raycast(touchOne.position, hits, TrackableType.Planes))
                {
                    Pose hitPose = hits[0].pose;
                    if(!isOverUI && isOver3DModel)
                    {
                        transform.position = hitPose.position;
                    }
                }
            }

            if(Input.touchCount == 2)
            {
                Touch touchTwo = Input.GetTouch(1);
                if(touchOne.phase == TouchPhase.Began || touchTwo.phase == TouchPhase.Began)
                {
                    initialTouchPos = touchTwo.position - touchOne.position;
                }

                if(touchOne.phase == TouchPhase.Moved || touchTwo.phase == TouchPhase.Moved)
                {
                    Vector2 currentTouchPos = touchTwo.position - touchOne.position;
                    float angle = Vector2.SignedAngle(initialTouchPos, currentTouchPos);
                    switch (detectionMode)
                    {
                        case PlaneDetectionMode.None:
                            break;
                        case PlaneDetectionMode.Horizontal:
                            aRPointer.transform.rotation = Quaternion.Euler(aRPointer.transform.eulerAngles.x, aRPointer.transform.eulerAngles.y - angle, aRPointer.transform.eulerAngles.z);
                            break;
                        case PlaneDetectionMode.Vertical:
                            aRPointer.transform.rotation = Quaternion.Euler(aRPointer.transform.eulerAngles.x, aRPointer.transform.eulerAngles.y, aRPointer.transform.eulerAngles.z - angle);
                            break;
                        default:
                            break;
                    }

                    //aRPointer.transform.rotation = Quaternion.Euler(aRPointer.transform.eulerAngles.x, aRPointer.transform.eulerAngles.y - angle, aRPointer.transform.eulerAngles.z);

                    initialTouchPos = currentTouchPos;
                }
            }

            if(isOver3DModel && item3DModel == null && !isOverUI)
            {
                GameManager.instance.ARPosition();
                item3DModel = itemSelected;
                itemSelected = null;
                aRPointer.SetActive(true);
                transform.position = item3DModel.transform.position;
                item3DModel.transform.parent = aRPointer.transform;
            }
        }
    }

    private bool IsTapOver3DModel(Vector2 touchPosition)
    {
        Ray ray = aRCamera.ScreenPointToRay(touchPosition);
        if(Physics.Raycast(ray, out RaycastHit hit3DModel))
        {
            if(hit3DModel.collider.CompareTag("Item"))
            {
                itemSelected = hit3DModel.transform.gameObject;
                return true;
            }

        }

        return false;
    }

    private bool IsTapOverUI(Vector2 touchPosition)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = new Vector2(touchPosition.x, touchPosition.y);

        List<RaycastResult> result = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, result);

        return result.Count > 0;
    }

    private void SetItemPosition()
    {
        if(item3DModel != null)
        {
            item3DModel.transform.parent = null;
            aRPointer.SetActive(false);
            item3DModel = null;
            transform.SetPositionAndRotation(Vector3.zero, Quaternion.Euler(Vector3.zero));
            LeanTween.rotateLocal(aRPointer, new Vector3(90f, 0f, 0f), 0f);
        }
    }

    public void DeleteItem()
    {
        //Destroy(item3DModel);
        item3DModel.SetActive(false);
        aRPointer.SetActive(false);
        transform.SetPositionAndRotation(Vector3.zero, Quaternion.Euler(Vector3.zero));
        LeanTween.rotateLocal(aRPointer, new Vector3(90f, 0f, 0f), 0f);
        GameManager.instance.MainMenu();
    }
}
