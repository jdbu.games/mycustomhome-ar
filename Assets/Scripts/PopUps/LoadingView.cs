using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingView : MonoBehaviour
{
    [SerializeField] RectTransform loadingPanel;
    [SerializeField] Image loadingImg;

    private void Awake()
    {
        EnableView(true);
    }

    public void EnableView(bool value)
    {
        gameObject.SetActive(value);
        loadingPanel.gameObject.SetActive(value);
        EnableLoadingBar(value);
    }

    public void EnableLoadingBar(bool value)
    {
        if (value)
        {
            print("OK");
            LeanTween.moveLocalX(loadingImg.gameObject, -450f, 0f);
            LeanTween.moveLocalX(loadingImg.gameObject, 450f, 3f).setLoopPingPong();
        }
        else
        {
            LeanTween.moveLocalX(loadingImg.gameObject, -450f, 0f);
            LeanTween.cancel(loadingImg.gameObject);
        }
    }
}
