using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AlertView : MonoBehaviour
{
    private TextMeshProUGUI title;
    private TextMeshProUGUI message;
    private Button closeBtn;

    private Action alertAction;

    public void Initialize(Alert alert, Action OnCompleted = null)
    {
        title = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        message = transform.GetChild(3).GetComponent<TextMeshProUGUI>();
        closeBtn = transform.GetChild(4).GetComponent<Button>();
        closeBtn.onClick.AddListener(Exit);

        title.text = alert.title;
        message.text = alert.message;

        if (OnCompleted != null)
        {
            alertAction = OnCompleted;
        }
    }


    public void Exit()
    {
        alertAction?.Invoke();
        Conclude();
        Destroy(this.gameObject);
        print("DESTROY");
    }

    public void Conclude()
    {
        closeBtn.onClick.RemoveListener(Exit);
    }
}
