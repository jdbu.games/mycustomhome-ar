﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

public class PopUpFactory
{
    private readonly PopUpConfigurations popUpConfigurations;

    public PopUpFactory(PopUpConfigurations newPopUpConfigurations)
    {
        popUpConfigurations = newPopUpConfigurations;
    }

    public AlertView CreateAlert(string code, Action OnCompleted = null)
    {
        var alert = popUpConfigurations.CreateAlertByCode(code, OnCompleted);
        return alert;
    }

    public LoadingView CreateLoading()
    {
        var loading = popUpConfigurations.CreateLoading();
        return loading;
    }


}
