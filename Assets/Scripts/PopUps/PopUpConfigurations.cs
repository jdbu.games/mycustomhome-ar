using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Custom/PopUp Configuration")]
public class PopUpConfigurations : ScriptableObject
{
    public string alertPrefabPath;
    public string loadingPrefabPath;
    public List<Alert> allAlerts;

    private AlertView alertView;
    private LoadingView loadingView;

    public AlertView CreateAlertByCode(string code, Action OnCompleted = null)
    {
        alertView = Instantiate(Resources.Load<AlertView>(alertPrefabPath));
        foreach (Alert alert in allAlerts)
        {
            if(alert.code == code)
            {
                alertView.Initialize(alert, OnCompleted);
                
            }
        }

        return alertView;
    }

    public LoadingView CreateLoading()
    {
        loadingView = Instantiate(Resources.Load<LoadingView>(loadingPrefabPath));
        return loadingView;
    }
}

[Serializable]
public class Alert
{
    //public AlertType alertType;
    public string code;
    public string title;
    public string message;
}
