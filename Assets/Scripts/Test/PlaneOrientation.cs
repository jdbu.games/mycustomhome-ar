using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PlaneOrientation : MonoBehaviour
{
    [SerializeField] private Button vertical;
    [SerializeField] private Button horizontal;
    [SerializeField] private Button all;

    [SerializeField] private ARPlaneManager aRPlaneManager;

    private void Start()
    {
        //SetButtons();
    }

    private void SetButtons()
    {
        vertical.onClick.AddListener(()=>ChangePlaneOrientation(0));
        horizontal.onClick.AddListener(()=>ChangePlaneOrientation(1));
        all.onClick.AddListener(()=>ChangePlaneOrientation(2));
    }

    public void ChangePlaneOrientation(int value)
    {
        switch (value)
        {
            case 0:
                aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.Vertical;
                break;

            case 1:
                aRPlaneManager.requestedDetectionMode = PlaneDetectionMode.Horizontal;
                break;

            case 2:
                aRPlaneManager.requestedDetectionMode = (PlaneDetectionMode)(-1);
                break;
        }
        
    }
}
